import React, { Component } from "react";
import {
  Jumbotron,
  Container,
  Iframe
} from "mdbreact";

class DondeEstamos extends Component {
  render() {
    return(
    <Container>
      <br></br>
      <br></br>
      <Jumbotron>
        <h2 >Estamos a la entrada de Espartinas. Con acceso a parking gratuito.</h2>
      <br></br>



        <hr className="my-2" />
      <br></br>

        <p>
        Nuestro horario es el siguiente:
        </p>
        <p>
        Lunes a viernes
        </p>
        <p>
        8:00 a 23:30
        </p>
        <p>
        Sábado, Domingo y Festivos
        </p>
        <p>
        9:00 a 00:30
        </p>        
      </Jumbotron>
      <Iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1652.8411769101363!2d-6.118719703585131!3d37.38280645065673!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd12139b106d660f%3A0x107a2c0520137206!2sBar+Por+Su+Sitio!5e0!3m2!1ses!2sse!4v1545036287974" />
    {/*    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1652.8411769101363!2d-6.118719703585131!3d37.38280645065673!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd12139b106d660f%3A0x107a2c0520137206!2sBar+Por+Su+Sitio!5e0!3m2!1ses!2sse!4v1545036287974" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>*/}
    </Container>
    );
  }
}

export default DondeEstamos;
