
import React, { Component } from "react";
import {
  Button,
  Col,
  Row,
  Container,
  Card,
  CardBody,
  CardGroup,
  CardImage,
  CardTitle,
  CardText,
  Modal,
  ModalBody,
  ModalHeader,
  ModalFooter,
  MDBBtn,
  Input,
  Iframe
} from "mdbreact";

class Recetasyvideos extends Component {

  state = {
    modal1: false,
    modal2: false,
    modal3: false,
    modal4: false,
    modal5: false,
    modal6: false,
    modal7: false,
    modal8: false,
    modal9: false,
    modal10: false,
    modal11: false,
    modal12: false,
    modal13: false,
    modal14: false,
    modal15: false,
    modal16: false,
    modal17: false,
    modal18: false,
    modal19: false,
    modal20: false,
    modal21: false
  };
  toggle(nr) {
    let modalNumber = "modal" + nr;
    this.setState({
      [modalNumber]: !this.state[modalNumber]
    });
  }
  render() {
    return (
      <Container>
        <h1 className="mt-5">En esta sección podrás ver cómo elaboramos nuestros platos.</h1>
        <h3 className="mt-2">¿Te atreves con alguno de ellos?</h3>
        <Container>
          <h5 className="mt-5" >Receta del mes - Solomillo a la mostaza</h5>
          <Row>
            <Col>
              <Card>
                <Iframe src="https://www.youtube.com/embed/NMq6uNKnlcg"  />
                <CardBody cascade>
                  <CardText>
                    Descubre cómo elaboramos nuestro delicioso solomillo 
                    con salsa de mostaza.
                  </CardText>
      <MDBBtn color="primary" onClick={() => this.toggle(8)}>Comentar</MDBBtn>
      <Modal isOpen={this.state.modal8} toggle={() => this.toggle(8)}>
          <ModalHeader toggle={() => this.toggle(8)}>Comentarios de la receta</ModalHeader>
          <ModalBody>
            <Container fluid className="black-white">
                <Row>[18/12/2018 19:12] Roberto: Hoy mismo la hago</Row>
                <Row>[17/12/2018 18:20] Jesús: Mi madre la hace mejor</Row>  
                <Row>[15/12/2018 14:32] María: Plato típico</Row>
                <br></br>
                <form className="mx-3 grey-text">
                <Input
                  label="Nombre"
                  icon="user"
                  group
                  type="text"
                  validate
                />
                <Input
                  type="textarea"
                  rows="2"
                  label="Mensaje"
                  icon="pencil"
                />
              </form>
            </Container>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => this.toggle(8)}>
              Volver
            </Button>{" "}
            <Button color="primary" onClick={() =>  this.toggle(8) & this.toggle(9)}>
          Añadir comentario
        </Button>          
        </ModalFooter>
        </Modal>
        <Modal
          isOpen={this.state.modal9}
          toggle={() => this.toggle(9)}
          centered
        >
          <ModalHeader toggle={() => this.toggle(9)}>Comentario añadido</ModalHeader>
          <ModalBody>
            Tu comentario ha sido añadido.
            <br></br>
            ¡Muchas gracias!
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => this.toggle(9) }>
              Cerrar
            </Button>
          </ModalFooter>
        </Modal>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <hr className="mt-5" />
          <h2 className="mt-5">Biblioteca de recetas</h2>

          <CardGroup deck className="mt-3">
            <Card>
              <CardImage
                src="http://www.demoslavueltaaldia.com/sites/default/files/styles/recetas_listado/public/natillas-colombianas.jpg?itok=fLvW3x_2"
                alt="Card image cap"
                top
                hover
                overlay="white-slight"
              />
              <CardBody>
                <CardTitle tag="h5">Natillas caseras</CardTitle>
                <CardText>
                  Sorprende a tus amigos con esta sencilla y riquísima 
                  elaboración. ¡La de toda la vida!
                </CardText>
                <Button onClick={() => this.toggle(2)} color="primary" size="md">
                  Reproducir
                </Button>
                <Modal
                      isOpen={this.state.modal2}
                      toggle={() => this.toggle(2)}
                      size="lg"
                    >
                      <ModalHeader toggle={() => this.toggle(2)}>Receta de Natilla</ModalHeader>
                      <ModalBody>
                      <Iframe src="https://www.youtube.com/embed/oauyX8Wec_U"  />
                      </ModalBody>
                      <ModalFooter>
                        <Button color="secondary" onClick={() => this.toggle(2)}>
                          Cerrar
                        </Button>
                      </ModalFooter>
                    </Modal>
      <MDBBtn color="primary" size="md" onClick={() => this.toggle(10)}>Comentar</MDBBtn>
      <Modal isOpen={this.state.modal10} toggle={() => this.toggle(10)}>
          <ModalHeader toggle={() => this.toggle(10)}>Comentarios de la receta</ModalHeader>
          <ModalBody>
            <Container fluid className="black-white">
                <Row>[18/12/2018 19:12] Roberto: Hoy mismo la hago</Row>
                <Row>[17/12/2018 18:20] Jesús: Mi madre la hace mejor</Row>  
                <Row>[15/12/2018 14:32] María: Plato típico</Row>
                <br></br>
                <form className="mx-3 grey-text">
                <Input
                  label="Nombre"
                  icon="user"
                  group
                  type="text"
                  validate
                />
                <Input
                  type="textarea"
                  rows="2"
                  label="Mensaje"
                  icon="pencil"
                />
              </form>
            </Container>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => this.toggle(10)}>
              Volver
            </Button>{" "}
            <Button color="primary" onClick={() =>  this.toggle(10) & this.toggle(11)}>
          Añadir comentario
        </Button>          
        </ModalFooter>
        </Modal>
        <Modal
          isOpen={this.state.modal11}
          toggle={() => this.toggle(11)}
          centered
        >
          <ModalHeader toggle={() => this.toggle(11)}>Comentario añadido</ModalHeader>
          <ModalBody>
            Tu comentario ha sido añadido.
            <br></br>
            ¡Muchas gracias!
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => this.toggle(11) }>
              Cerrar
            </Button>
          </ModalFooter>
        </Modal>
              </CardBody>
            </Card>

            <Card>
              <CardImage
                src="https://blog.divarian.com/wp-content/uploads/blog-anida-bbva-vivienda-salmorejo-600x400.jpg"
                alt="Card image cap"
                top
                hover
                overlay="white-slight"
              />
              <CardBody>
                <CardTitle tag="h5">Salmorejo Cordobés</CardTitle>
                <CardText>
                  ¿A quién le apetece un salmorejo fesquito?
                  Mira cómo lo hacemos en Por su Sitio...
                </CardText>
                <Button onClick={() => this.toggle(3)} color="primary" size="md">
                  Reproducir
                </Button>
                <Modal
                      isOpen={this.state.modal3}
                      toggle={() => this.toggle(3)}
                      size="lg"
                    >
                      <ModalHeader toggle={() => this.toggle(3)}>Receta de salmorejo</ModalHeader>
                      <ModalBody>
                      <Iframe src="https://www.youtube.com/embed/_mb2N9apN2A"  />
                      </ModalBody>
                      <ModalFooter>
                        <Button color="primary" onClick={() => this.toggle(3)}>
                          Cerrar
                        </Button>
                      </ModalFooter>
                    </Modal>
                    <MDBBtn color="primary" size="md" onClick={() => this.toggle(12)}>Comentar</MDBBtn>
      <Modal isOpen={this.state.modal12} toggle={() => this.toggle(12)}>
          <ModalHeader toggle={() => this.toggle(12)}>Comentarios de la receta</ModalHeader>
          <ModalBody>
            <Container fluid className="black-white">
                <Row>[18/12/2018 19:12] Roberto: Hoy mismo la hago</Row>
                <Row>[17/12/2018 18:20] Jesús: Mi madre la hace mejor</Row>  
                <Row>[15/12/2018 14:32] María: Plato típico</Row>
                <br></br>
                <form className="mx-3 grey-text">
                <Input
                  label="Nombre"
                  icon="user"
                  group
                  type="text"
                  validate
                />
                <Input
                  type="textarea"
                  rows="2"
                  label="Mensaje"
                  icon="pencil"
                />
              </form>
            </Container>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => this.toggle(12)}>
              Volver
            </Button>{" "}
            <Button color="primary" onClick={() =>  this.toggle(12) & this.toggle(13)}>
          Añadir comentario
        </Button>          
        </ModalFooter>
        </Modal>
        <Modal
          isOpen={this.state.modal13}
          toggle={() => this.toggle(13)}
          centered
        >
          <ModalHeader toggle={() => this.toggle(13)}>Comentario añadido</ModalHeader>
          <ModalBody>
            Tu comentario ha sido añadido.
            <br></br>
            ¡Muchas gracias!
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => this.toggle(13) }>
              Cerrar
            </Button>
          </ModalFooter>
        </Modal>
              </CardBody>
            </Card>

            <Card>
              <CardImage
                src="http://loribarber.com/wp-content/uploads/2010/10/IMGP2624-1024x7681-600x400.jpg"
                alt="Card image cap"
                top
                hover
                overlay="white-slight"
              />
              <CardBody>
                <CardTitle tag="h5">Tortilla de patatas al Whisky</CardTitle>
                <CardText>
                  Si te quedaba esta tortilla por probar ahora la puedes
                  hacer en casa.
                </CardText>
                <Button onClick={() => this.toggle(4)} color="primary" size="md">
                  Reproducir
                </Button>
                <Modal
                      isOpen={this.state.modal4}
                      toggle={() => this.toggle(4)}
                      size="lg"
                    >
                      <ModalHeader toggle={() => this.toggle(4)}>Receta de tortilla de patatas</ModalHeader>
                      <ModalBody>
                      <Iframe src="https://www.youtube.com/embed/n7elkQ30JxU"  />
                      </ModalBody>
                      <ModalFooter>
                        <Button color="secondary" onClick={() => this.toggle(4)}>
                          Cerrar
                        </Button>
                      </ModalFooter>
                    </Modal>
                    <MDBBtn color="primary" size="md" onClick={() => this.toggle(14)}>Comentar</MDBBtn>
      <Modal isOpen={this.state.modal14} toggle={() => this.toggle(14)}>
          <ModalHeader toggle={() => this.toggle(14)}>Comentarios de la receta</ModalHeader>
          <ModalBody>
            <Container fluid className="black-white">
                <Row>[18/12/2018 19:12] Roberto: Hoy mismo la hago</Row>
                <Row>[17/12/2018 18:20] Jesús: Mi madre la hace mejor</Row>  
                <Row>[15/12/2018 14:32] María: Plato típico</Row>
                <br></br>
                <form className="mx-3 grey-text">
                <Input
                  label="Nombre"
                  icon="user"
                  group
                  type="text"
                  validate
                />
                <Input
                  type="textarea"
                  rows="2"
                  label="Mensaje"
                  icon="pencil"
                />
              </form>
            </Container>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => this.toggle(14)}>
              Volver
            </Button>{" "}
            <Button color="primary" onClick={() =>  this.toggle(14) & this.toggle(15)}>
          Añadir comentario
        </Button>          
        </ModalFooter>
        </Modal>
        <Modal
          isOpen={this.state.modal15}
          toggle={() => this.toggle(15)}
          centered
        >
          <ModalHeader toggle={() => this.toggle(15)}>Comentario añadido</ModalHeader>
          <ModalBody>
            Tu comentario ha sido añadido.
            <br></br>
            ¡Muchas gracias!
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => this.toggle(15) }>
              Cerrar
            </Button>
          </ModalFooter>
        </Modal>
              </CardBody>
            </Card>
          </CardGroup>

          <CardGroup deck className="mt-3">
            <Card>
              <CardImage
                src="https://altacocinaencasa.com/wp-content/uploads/presentacion-morcillo-a-baja-temperatura-con-salsa-bordalesa-600x400.jpg"
                alt="Card image cap"
                top
                hover
                overlay="white-slight"
              />
              <CardBody>
                <CardTitle tag="h5">Carrillá ibérica</CardTitle>
                <CardText>
                  En Por su Sitio te traemos los sabores del campo.
                  Mira cómo lo hacemos y disfruta.
                </CardText>
                <Button onClick={() => this.toggle(5)} color="primary" size="md">
                  Reproducir
                </Button>
                <Modal
                      isOpen={this.state.modal5}
                      toggle={() => this.toggle(5)}
                      size="lg"
                    >
                      <ModalHeader toggle={() => this.toggle(5)}>Receta de carrillá</ModalHeader>
                      <ModalBody>
                      <Iframe src="https://www.youtube.com/embed/5MB7NBOTCO0"  />
                      </ModalBody>
                      <ModalFooter>
                        <Button color="secondary" onClick={() => this.toggle(5)}>
                          Cerrar
                        </Button>
                      </ModalFooter>
                    </Modal>
                    <MDBBtn color="primary" size="md" onClick={() => this.toggle(16)}>Comentar</MDBBtn>
      <Modal isOpen={this.state.modal16} toggle={() => this.toggle(16)}>
          <ModalHeader toggle={() => this.toggle(16)}>Comentarios de la receta</ModalHeader>
          <ModalBody>
            <Container fluid className="black-white">
                <Row>[18/12/2018 19:12] Roberto: Hoy mismo la hago</Row>
                <Row>[17/12/2018 18:20] Jesús: Mi madre la hace mejor</Row>  
                <Row>[15/12/2018 14:32] María: Plato típico</Row>
                <br></br>
                <form className="mx-3 grey-text">
                <Input
                  label="Nombre"
                  icon="user"
                  group
                  type="text"
                  validate
                />
                <Input
                  type="textarea"
                  rows="2"
                  label="Mensaje"
                  icon="pencil"
                />
              </form>
            </Container>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => this.toggle(16)}>
              Volver
            </Button>{" "}
            <Button color="primary" onClick={() =>  this.toggle(16) & this.toggle(17)}>
          Añadir comentario
        </Button>          
        </ModalFooter>
        </Modal>
        <Modal
          isOpen={this.state.modal17}
          toggle={() => this.toggle(17)}
          centered
        >
          <ModalHeader toggle={() => this.toggle(17)}>Comentario añadido</ModalHeader>
          <ModalBody>
            Tu comentario ha sido añadido.
            <br></br>
            ¡Muchas gracias!
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => this.toggle(17) }>
              Cerrar
            </Button>
          </ModalFooter>
        </Modal>
              </CardBody>
            </Card>

            <Card>
              <CardImage
                src="http://www.cosasdecome.es/wp-content/uploads/2012/09/bocadillo-de-pring%C3%A1.jpg"
                alt="Card image cap"
                top
                hover
                overlay="white-slight"
              />
              <CardBody>
                <CardTitle tag="h5">Pringá en montadito</CardTitle>
                <CardText>
                  La pringá de toda la vida metida en pan.
                  ¿Seguro que te puedes resistir?
                </CardText>
                <Button onClick={() => this.toggle(6)} color="primary" size="md">
                  Reproducir
                </Button>
                <Modal
                      isOpen={this.state.modal6}
                      toggle={() => this.toggle(6)}
                      size="lg"
                    >
                      <ModalHeader toggle={() => this.toggle(6)}>Receta de pringá</ModalHeader>
                      <ModalBody>
                      <Iframe src="https://www.youtube.com/embed/8aJBCLLsPD4"  />
                      </ModalBody>
                      <ModalFooter>
                        <Button color="secondary" onClick={() => this.toggle(6)}>
                          Cerrar
                        </Button>
                      </ModalFooter>
                    </Modal>
                    <MDBBtn color="primary" size="md" onClick={() => this.toggle(18)}>Comentar</MDBBtn>
      <Modal isOpen={this.state.modal18} toggle={() => this.toggle(18)}>
          <ModalHeader toggle={() => this.toggle(18)}>Comentarios de la receta</ModalHeader>
          <ModalBody>
            <Container fluid className="black-white">
                <Row>[18/12/2018 19:12] Roberto: Hoy mismo la hago</Row>
                <Row>[17/12/2018 18:20] Jesús: Mi madre la hace mejor</Row>  
                <Row>[15/12/2018 14:32] María: Plato típico</Row>
                <br></br>
                <form className="mx-3 grey-text">
                <Input
                  label="Nombre"
                  icon="user"
                  group
                  type="text"
                  validate
                />
                <Input
                  type="textarea"
                  rows="2"
                  label="Mensaje"
                  icon="pencil"
                />
              </form>
            </Container>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => this.toggle(18)}>
              Volver
            </Button>{" "}
            <Button color="primary" onClick={() =>  this.toggle(18) & this.toggle(19)}>
          Añadir comentario
        </Button>          
        </ModalFooter>
        </Modal>
        <Modal
          isOpen={this.state.modal19}
          toggle={() => this.toggle(19)}
          centered
        >
          <ModalHeader toggle={() => this.toggle(19)}>Comentario añadido</ModalHeader>
          <ModalBody>
            Tu comentario ha sido añadido.
            <br></br>
            ¡Muchas gracias!
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => this.toggle(19) }>
              Cerrar
            </Button>
          </ModalFooter>
        </Modal>
              </CardBody>
            </Card>

            <Card>
              <CardImage
                src="http://www.record.com.pe/consejos/wp-content/uploads/2017/09/arroz-con-leche-600x400.jpg"
                alt="Card image cap"
                top
                hover
                overlay="white-slight"
              />
              <CardBody>
                <CardTitle tag="h5">Arroz con leche</CardTitle>
                <CardText>
                  Hoy no te quedas sin postre antes de dormir.
                  ¿Te lo llevamos a casa o lo haces tú?
                </CardText>
                <Button onClick={() => this.toggle(7)} color="primary" size="md">
                  Reproducir
                </Button>
                <Modal
                      isOpen={this.state.modal7}
                      toggle={() => this.toggle(7)}
                      size="lg"
                    >
                      <ModalHeader toggle={() => this.toggle(7)}>Receta de Arroz con leche</ModalHeader>
                      <ModalBody>
                      <Iframe src="https://www.youtube.com/embed/xQsPiAf-t1Q"  />
                      </ModalBody>
                      <ModalFooter>
                        <Button color="secondary" onClick={() => this.toggle(7)}>
                          Cerrar
                        </Button>
                      </ModalFooter>
                    </Modal>
                <MDBBtn color="primary" size="md" onClick={() => this.toggle(20)}>Comentar</MDBBtn>
      <Modal isOpen={this.state.modal20} toggle={() => this.toggle(20)}>
          <ModalHeader toggle={() => this.toggle(20)}>Comentarios de la receta</ModalHeader>
          <ModalBody>
            <Container fluid className="black-white">
                <Row>[18/12/2018 19:12] Roberto: Hoy mismo la hago</Row>
                <Row>[17/12/2018 18:20] Jesús: Mi madre la hace mejor</Row>  
                <Row>[15/12/2018 14:32] María: Plato típico</Row>
                <br></br>
                <form className="mx-3 grey-text">
                <Input
                  label="Nombre"
                  icon="user"
                  group
                  type="text"
                  validate
                />
                <Input
                  type="textarea"
                  rows="2"
                  label="Mensaje"
                  icon="pencil"
                />
              </form>
            </Container>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => this.toggle(20)}>
              Volver
            </Button>{" "}
            <Button color="primary" onClick={() =>  this.toggle(20) & this.toggle(21)}>
          Añadir comentario
        </Button>          
        </ModalFooter>
        </Modal>
        <Modal
          isOpen={this.state.modal21}
          toggle={() => this.toggle(21)}
          centered
        >
          <ModalHeader toggle={() => this.toggle(21)}>Comentario añadido</ModalHeader>
          <ModalBody>
            Tu comentario ha sido añadido.
            <br></br>
            ¡Muchas gracias!
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => this.toggle(21) }>
              Cerrar
            </Button>
          </ModalFooter>
        </Modal>
              </CardBody>
            </Card>
          </CardGroup>
        </Container>
      </Container>
    );
  }
}

export default Recetasyvideos;
