# Trabajo realizado por Roberto Mantas Nakhai y Jesús Díaz Cabello
## Para ver la página web montada visitar: http://130.240.5.36:3000
# Página Web de Bar de Tapas Por Su Sitio
Página creada con React y material design. 
Asignatura: Diseño Guiado por Experiencia de Usuario-Máster Universitario en Ingeniería Informática
Profesor: Pablo Trinidad Martín-Arroyo

## Comienza la instalación
Para hacer funcionar este repositorio en tu ordenador tienes que seguir los siguiente pasos:

- clona el repository

- cd al directorio con el repositorio

- comando `yarn install` (con `npm install` debería funcionar pero a veces da error por lo que recomiendo usar `yarn`)

- ahora ponerlo a correr con `yarn start` (o `npm start` si te ha funcionado `npm`antes)

- Para construir el proyecto usar `yarn run build` (od `npm run build`)

- Debe estar funcionando!

## Bugs
Si tienes algún problema coméntalo con un ticket en el apartado issues

Si quieres ayudar en mejorar en algún aspecto la página también estamos abierto a ello
